def inputValue():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return inputValue()


if __name__ == "__main__":
    x = inputValue()
    diviseurs = []
    for i in range(1, x + 1):
        if x % i == 0:
            diviseurs.append(i)
    print(diviseurs)
