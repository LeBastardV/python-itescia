def Superiority():
    try:
        a = int(input('a: '))
        b = int(input('b: '))
        if a > b:
            print("a supérieur à b")
        elif a == b:
            print("a égal b")
        else:
            print("b supérieur à a")
    except:
        print("Merci de fournir des chiffres ou des nombres uniquement")
        Superiority()


if __name__ == '__main__':
    Superiority()
