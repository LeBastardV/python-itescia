def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


if __name__ == "__main__":
    string = inputString()
    firstCaract = string[0]
    lastCaract = string[len(string)-1]
    newString = lastCaract + string[1:-1] + firstCaract
    print(newString)
