def nombreOccurences(L, x):
    counter = 0
    if x in L:
        counter += 1
    return counter


if __name__ == "__main__":
    x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    print(nombreOccurences(x, 2))
