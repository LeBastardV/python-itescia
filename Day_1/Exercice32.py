def eachPermutation(L):
    for index in range(0, len(L)):  # Nombre de listes possible
        result = []
        for index2 in range(0, len(L)):
            result.append(L[(index2+index) % len(L)])
        print(result)


if __name__ == "__main__":
    x = [0, 1, 2, 3, 4, 5, 6, 7]
    eachPermutation(x)
