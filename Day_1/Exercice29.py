def deleteDuplicates(List):
    newList = []
    for element in List:
        if element not in newList:
            newList.append(element)
    return newList


if __name__ == "__main__":
    fruits = ['apple', 'banana', 'banana',
              'apple', 'grape', 'kiwi', 'orange', 'kiwi']
    print(deleteDuplicates(fruits))
