import math


def surfaceEtPerimetre():
    try:
        rayon = int(input('Rayon du cercle : '))
        perimetre = 2 * math.pi * rayon
        surface = math.pi * rayon * rayon
        print("Perimetre : ", perimetre)
        print("Surface : ", surface)
    except:
        print("Merci de fournir un chiffre ou nombre correct")


if __name__ == '__main__':
    surfaceEtPerimetre()
