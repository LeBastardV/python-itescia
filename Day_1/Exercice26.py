def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def splitString(string):
    return string.split()


def searchFirstChar(char, splitedString):
    wordWithChar = []
    for word in splitedString:
        if word[0] == char:
            wordWithChar.append(word)
    return wordWithChar


if __name__ == "__main__":
    string = inputString()
    print(searchFirstChar('a', splitString(string)))
