def YearsOld():
    try:
        a = int(input('a: '))
        if a >= 18:
            print("Vous êtes Majeur !")
        elif a < 0:
            print("Merci de fournir un age correct")
            YearsOld()
        else:
            print("Vous n'êtes pas Majeur !")
    except:
        print("Merci de fournir un age correct")
        YearsOld()


if __name__ == '__main__':
    YearsOld()
