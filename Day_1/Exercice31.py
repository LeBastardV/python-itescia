def pairOrImpaire(List):
    listePair = []
    listeImpaire = []
    for element in List:
        try:
            if int(element) % 2 == 0:
                listePair.append(element)
            else:
                listeImpaire.append(element)
        except:
            return "Tous les elements de la liste fournie ne sont pas de nombres"
    return listePair, listeImpaire


if __name__ == "__main__":
    numbers = [1, 2, 3, 4, 5, 6, 7, 8]
    print(pairOrImpaire(numbers))
