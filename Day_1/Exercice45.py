def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def getUpperLower(string):
    lowers = 0
    uppers = 0

    for char in string:
        if char.isupper():
            uppers += 1
        if char.islower():
            lowers += 1

    return lowers, uppers


if __name__ == "__main__":
    string = inputString()
    print(getUpperLower(string))
