def joinCommonElement(list1, list2):
    ensemble = set()
    for element in list1:
        if element in list2:
            ensemble.add(element)
    return ensemble


if __name__ == "__main__":
    fruits = ['apple', 'banana', 'kiwi', 'orange']
    fruits2 = ['pear', 'grappe', 'orange', 'peach']
    print(joinCommonElement(fruits, fruits2))
