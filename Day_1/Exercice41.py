def nombreDivisibles(list1, diviseur):
    counter = 0
    for number in list1:
        if number % diviseur == 0:
            counter += 1
    return counter


if __name__ == "__main__":
    x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    print(nombreDivisibles(x, 2))
