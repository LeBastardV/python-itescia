def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


if __name__ == "__main__":
    chaine = inputString()
    for i in chaine:
        print("Le caractere : ", i, " figure ",
              chaine.count(i), "dans la chaine s.")
