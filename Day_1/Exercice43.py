def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def InsertEtoile(string):
    newString = ""
    for index in range(0, len(string)):
        if index != len(string) - 1:
            newString = newString + string[index] + "*"
        else:
            newString = newString + string[index]

    return newString


if __name__ == "__main__":
    string = inputString()
    print(InsertEtoile(string))
