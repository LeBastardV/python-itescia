def inputValue():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return inputValue()


def multiplication(x):
    for i in range(1, 11):
        print(x, "x", i, "=", x*i)


if __name__ == "__main__":
    #x = inputValue()
    for i in range(1, 10):
        multiplication(i)
        print("\n")
