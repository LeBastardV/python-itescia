def isEmpty(string):
    if len(string) == 0:
        return True
    return False


if __name__ == "__main__":
    print(isEmpty(['pomme']))  # False
    print(isEmpty([]))  # True
    print(isEmpty('pomme'))  # False
    print(isEmpty(''))  # True
