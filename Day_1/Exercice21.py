def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def countVowel(string):
    vowelList = ['a', 'e', 'i', 'o', 'u', 'y']
    counter = 0
    for char in string:
        if char in vowelList:
            counter += 1
    return counter


if __name__ == "__main__":
    string = inputString()
    print("Il y a ", countVowel(string), "voyelle dans ", string)
