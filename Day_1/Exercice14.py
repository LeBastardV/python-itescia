import math


def inputValue():
    try:
        x = int(input('Saisir un nombre entier positif : '))
        if x >= 0:
            return x
        else:
            print("Ce n'est pas un nombre entier positif")
            return inputValue()
    except:
        print("La valeur n'est pas un nombre")
        return inputValue()


def carreParfait(x):
    racine = math.sqrt(x)
    if racine == int(racine):
        print("Carré parfait")
    else:
        print("Carré non parfait")


if __name__ == "__main__":
    x = inputValue()
    carreParfait(x)
