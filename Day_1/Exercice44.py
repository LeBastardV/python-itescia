def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def toutEnMajuscule(L):
    newList = []
    for element in L:
        newList.append(element.upper())
    return newList


if __name__ == "__main__":
    fruits = ['apple', 'banana', 'kiwi', 'orange']
    print(toutEnMajuscule(fruits))
