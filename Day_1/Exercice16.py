def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def afficherCaractère(chaine):
    length = len(chaine)
    for i in range(0, length):
        print(chaine[i])


if __name__ == "__main__":
    chaine = inputString()
    afficherCaractère(chaine)
