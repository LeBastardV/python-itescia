def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


if __name__ == "__main__":
    chaine = inputString()
    counterA = 0
    for i in chaine:
        if i == 'a':
            counterA += 1

    print("Le caractere 'a' apparait ", counterA, "fois")
