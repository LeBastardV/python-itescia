def commonWord(string1, string2):
    listResult = []
    for element in string1.split():
        if element in string2.split() and element not in listResult:
            listResult.append(element)
    return listResult


if __name__ == "__main__":
    fruits = 'apple apple banana kiwi orange peach'
    fruits2 = 'pear grappe orange peach apple'
    print(commonWord(fruits, fruits2))
