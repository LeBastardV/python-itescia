def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def returnPairIndex(string):
    newstring = ""
    for index in range(0, len(string)):
        if index % 2 == 0:
            newstring += string[index]
    return newstring


if __name__ == "__main__":
    string = inputString()
    print(returnPairIndex(string))
