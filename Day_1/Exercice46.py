def convertNumber(number):
    if number == 0:
        return [0]

    numberlist = []
    while number > 0:
        numberlist.append(number % 10)
        number = number // 10
    return numberlist[::-1]


if __name__ == "__main__":
    print(convertNumber(358))
    # sortie [3, 5, 8]
