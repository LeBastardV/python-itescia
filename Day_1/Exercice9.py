def factorielle():
    try:
        a = int(input('Factorielle de : '))
        result = 1
        for i in range(1, a+1):
            result = result * i
        print(result)
    except:
        print("Merci de fournir un chiffre ou nombre correct")


if __name__ == '__main__':
    factorielle()
