import re


def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def deleteMultipleSpace(string):
    return re.sub(' +', ' ', string)


if __name__ == "__main__":
    string = inputString()
    print(deleteMultipleSpace(string))
