def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def searchLongestWord(string):
    splitedString = string.split()
    maxChar = 0
    longestWordIndex = 0
    for index in range(0, len(splitedString)):
        if len(splitedString[index]) >= maxChar:
            maxChar = len(splitedString[index])
            longestWordIndex = index
    return splitedString[longestWordIndex]


if __name__ == "__main__":
    string = inputString()
    print(searchLongestWord(string))
