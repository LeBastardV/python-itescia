import math


def inputValue():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return inputValue()


def PrimeNumber(x):
    diviseurs = []
    for i in range(1, x+1):
        if x % i == 0:
            diviseurs.append(i)

    if len(diviseurs) == 2 or x == 1:
        print("Prime Number")
    else:
        print("Not Prime Number")


if __name__ == "__main__":
    x = inputValue()
    PrimeNumber(x)
