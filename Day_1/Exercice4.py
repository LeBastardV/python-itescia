def PairImpair():
    try:
        a = int(input('a: '))
        if a % 2 == 0:
            print("Le nombre est pair")
        else:
            print("Le nombre est impair")
    except:
        print("Merci de fournir des chiffres ou des nombres uniquement")
        PairImpair()


if __name__ == '__main__':
    PairImpair()
