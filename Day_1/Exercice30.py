def commonElement(list1, list2):
    for element in list1:
        if element in list2:
            return True
    return False


if __name__ == "__main__":
    fruits = ['apple', 'banana', 'kiwi', 'orange']
    fruits2 = ['pear', 'grappe', 'orange', 'peach']
    print(commonElement(fruits, fruits2))
