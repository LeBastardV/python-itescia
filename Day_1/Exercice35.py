def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def countWordOccur(string):
    listWord = string.split()
    dictionnary = {}
    for word in listWord:
        if word in dictionnary:
            dictionnary[word] += 1
        else:
            dictionnary[word] = 1
    return dictionnary


if __name__ == "__main__":
    string = inputString()
    print(countWordOccur(string))
