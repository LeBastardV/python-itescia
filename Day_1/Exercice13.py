def inputValue():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return inputValue()


def divisionEuclidienne(dividende, diviseur):
    quotient = dividende // diviseur
    reste = dividende % diviseur
    print("quotient: ", quotient, " reste : ", reste)


if __name__ == "__main__":
    dividende = inputValue()
    diviseur = inputValue()
    divisionEuclidienne(dividende, diviseur)
