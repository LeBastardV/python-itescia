def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def palindrome(string):
    # Reverse la chaine et la comparer
    if string == string[::-1]:
        return "Palindrome"
    else:
        return "Pas un palindrome"


if __name__ == "__main__":
    string = inputString()
    print(palindrome(string))
