def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


def inverteLastFirst(string):  # supposé avoir des espace entre chaque mots
    splittedString = string.split()
    tmp = splittedString[-1]
    splittedString[-1] = splittedString[0]
    splittedString[0] = tmp
    return " ".join(splittedString)


if __name__ == "__main__":
    string = inputString()

    print(inverteLastFirst(string))
