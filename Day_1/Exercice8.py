def sum():
    try:
        a = int(input('Nombre: '))
        result = 0
        for i in range(1, a+1):
            result = result + i
        print(result)
    except:
        print("Merci de fournir un chiffre correct")


if __name__ == '__main__':
    sum()
