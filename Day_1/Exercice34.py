def overAverage(List):
    overAverage = []
    for element in List:
        try:
            if int(element) >= 10:
                overAverage.append(element)
        except:
            return "Tous les elements de la liste fournie ne sont pas de nombres"
    return overAverage


if __name__ == "__main__":
    notes = [12, 4, 14, 11, 18, 13, 7, 10, 5, 9, 15, 8, 14, 16]
    print(overAverage(notes))
