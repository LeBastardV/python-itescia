def inputString():
    try:
        return input('Saisir une chaine : ')
    except:
        print("Incorrect")
        return inputString()


if __name__ == "__main__":
    string = inputString()

    print(len(string.split()))
