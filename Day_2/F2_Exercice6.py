from itertools import zip_longest


class Poly:
    def __init__(self, *args):
        self.listeCoefficients = []
        for coefficient in args:
            self.listeCoefficients.append(coefficient)

    def coeff(self):
        print(self.listeCoefficients)

    def evalue(self, x):
        result = 0
        for puissance, value in enumerate(self.listeCoefficients):
            result += ((x**puissance)*value)
        return result

    def __add__(self, secondPolynome):
        listeTupleCoefficient = list(zip_longest(self.listeCoefficients,
                                                 secondPolynome.listeCoefficients, fillvalue=0))

        listNewCoeffPolynome = []

        for tupleValue in listeTupleCoefficient:
            newCoefficient = 0
            for coefficient in tupleValue:
                newCoefficient += coefficient

            listNewCoeffPolynome.append(newCoefficient)

        return Poly(*listNewCoeffPolynome)

    def __mul__(self, secondPolynome):
        if len(self.listeCoefficients) > len(secondPolynome.listeCoefficients):
            grandeListe = self.listeCoefficients
            petiteListe = secondPolynome.listeCoefficients
        else:
            petiteListe = self.listeCoefficients
            grandeListe = secondPolynome.listeCoefficients

        puissanceMax = len(grandeListe)-1+len(petiteListe)-1
        listNewCoeffPolynome = []
        for _ in range(puissanceMax+1):
            listNewCoeffPolynome.append(0)

        for puissanceGdListe, coeffGdListe in enumerate(grandeListe):
            for puissancePetiteListe, coeffPetiteListe in enumerate(petiteListe):
                puissance = puissanceGdListe+puissancePetiteListe
                listNewCoeffPolynome[puissance] += coeffGdListe * \
                    coeffPetiteListe
        return Poly(*listNewCoeffPolynome)


if __name__ == "__main__":
    p1 = Poly(1, 4)
    p1.coeff()  # [1, 4]
    print("p(0): ", p1.evalue(0))  # p(0)=  1
    print("p(1): ", p1.evalue(1))  # p(0)=  5
    p2 = Poly(3, -3, 2)  # [3, -3, 2]
    p2.coeff()
    print("p(0): ", p2.evalue(0))  # p(0)=  3
    print("p(1): ", p2.evalue(1))  # p(0)=  2
    p3 = p1 + p2
    p3.coeff()  # [4, 1, 2]
    p4 = p1 * p2
    p4.coeff()  # [3, 9, -10, 8]
