# Écrivez une classe Fraction qui crée des objets initialisés avec deux nombres entiers
# .num et .denom pour le numérateur et le dénominateur.
# Ajoutez une méthode .affiche() qui affiche une représentation de la fraction.
# Ajoutez des méthodes spéciales pour pouvoir utiliser les opérateurs +, -, *, /.
# Pour réduire la fraction, vous pouvez employer la fonction math.gcd(a, b) du module math,
# qui calcule le plus grand commun diviseur entre deux entiers a et b.
import math


class Fraction:
    def __init__(self, num, denom):
        self.num = num
        self.denom = denom

    def affiche(self):
        print(self.num, "/", self.denom, sep="")

    def __add__(self, fractionToAdd):
        result = Fraction(self.num*fractionToAdd.denom +
                          fractionToAdd.num*self.denom, self.denom*fractionToAdd.denom)
        pgcd = math.gcd(result.num, result.denom)
        return Fraction(result.num // pgcd, result.denom // pgcd)

    def __sub__(self, fractionTosub):
        result = Fraction(self.num*fractionTosub.denom -
                          fractionTosub.num*self.denom, self.denom*fractionTosub.denom)
        pgcd = math.gcd(result.num, result.denom)
        return Fraction(result.num // pgcd, result.denom // pgcd)

    def __mul__(self, fractionToMul):
        result = Fraction(self.num*fractionToMul.num,
                          self.denom*fractionToMul.denom)
        pgcd = math.gcd(result.num, result.denom)
        return Fraction(result.num // pgcd, result.denom // pgcd)

    def __truediv__(self, fractionTotruediv):
        result = Fraction(self.num*fractionTotruediv.denom,
                          self.denom*fractionTotruediv.num)
        pgcd = math.gcd(result.num, result.denom)
        return Fraction(result.num // pgcd, result.denom // pgcd)

    def __repr__(self):
        return "Numérateur: {self.num}, Denominateur: {self.denom}".format(self=self)


if __name__ == "__main__":
    f = Fraction(3, 4)
    f.affiche()
    # 3/4
    g = Fraction(1, 2)
    g.affiche()
    # 1/2
    r1 = f + g
    r1.affiche()
    # 5/4
    r2 = f / g
    r2.affiche()
    # 3/2
    r3 = f - g
    r3.affiche()
    # 1/4
    r4 = f * g
    r4.affiche()
    # 3/8
    print(r4)
