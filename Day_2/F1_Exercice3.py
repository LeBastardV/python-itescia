# Dans le jargon combinatoire, le terme "powerset" d'un ensemble A,
# signifie l'ensemble de tous les sous-ensembles de A.
# Si on représente les ensembles par les listes, disons A=[1,2,3],
# alors powerset(A) devra retourner : [[], [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3]].
# Construire un tel générateur, qui engendre tous les éléments du powerset.

import itertools


def powerset(A):
    powerset = []
    for temp in range(len(A) + 1):
        combination = itertools.combinations(A, temp)
        combi_list = list(combination)
        powerset += combi_list
    return powerset


if __name__ == "__main__":
    "powerset([1,2,3]) --> [(), (1,), (2,), (3,), (1, 2), (1, 3), (2, 3), (1, 2, 3)]"
    print(powerset([1, 2, 3]))
