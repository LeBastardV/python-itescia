# Generateurs.
# Écrivez une classe TableMultiplication qui crée des objets initialisés avec un nombre entier.
# Ajouter une méthode .prochain() qui renvoie à chaque appel un nouveau terme de la
# table de multiplication correspondante.

# >>> tab = TableMultiplication(3)
# >>> tab.prochain()
# 0
# >>> tab.prochain()
# 3
# >>> tab.prochain()
# 6
# >>> tab.prochain()
# 9

class TableMultiplication:
    def __init__(self, nombreEntier):
        self.nombreEntier = nombreEntier
        self.last = 0

    def prochain(self):
        print(self.nombreEntier * self.last)
        self.last += 1

    def __repr__(self):
        return "Nombre entier Table: {self.nombreEntier}, Dernier Enuméré: {self.last}".format(self=self)


if __name__ == "__main__":
    tab = TableMultiplication(3)
    tab.prochain()
    tab.prochain()
    tab.prochain()
    tab.prochain()
    print(tab)
