# Écrivez une classe CompteBancaire.
# Les objets sont initialisés avec le nom du titulaire et le solde.
# L’argument solde doit être facultatif et avoir une valeur prédéfinie zéro.
# Ajoutez deux méthodes .depot(somme) et .retrait(somme) pour changer le solde.
# Ajoutez une méthode .affiche() qui montre le solde courant.

# >>> compte1 = CompteBancaire(’Jean’, 1000)
# >>> compte1.retrait(200)
# >>> compte1.affiche()
# Le solde du compte de Jean est 800 euros.
# >>> compte2 = CompteBancaire(’Marc’)
# >>> compte2.depot(500)
# >>> compte2.affiche()
# Le solde du compte de Marc est 500 euros.

class CompteBancaire:
    def __init__(self, nomTitulaire, solde=0):
        self.titulaire = nomTitulaire
        self.solde = solde

    def depot(self, somme):
        self.solde += somme

    def retrait(self, somme):
        self.solde -= somme

    def affiche(self):
        print("Le compte de ", self.titulaire, "est", self.solde)

    def __repr__(self):
        return "Titulaire: {self.titulaire}, Solde: {self.solde}".format(self=self)


if __name__ == "__main__":
    compteValentin = CompteBancaire("Valentin", 2500)
    compteValentin.affiche()
    compteValentin.depot(500)
    compteValentin.affiche()
    compteValentin.retrait(3000)
    compteValentin.affiche()

    print(compteValentin)
