# Étant donné un générateur g qui fournit la séquence : a, b, c, d, e, ...,
# construire un générateur g2 qui donne a, a, b, b, c, c, d, d, e, e, ...
# Généraliser pour la réplication non pas deux, mais un nombre arbitraire, paramétrable, de fois.
# Itérer l'original g dans g2.

def générateurInitial():
    return list(map(chr, range(97, 123)))


def duplicateur(nb):
    alphabet = générateurInitial()
    newAlphabet = []
    for letter in alphabet:
        for _ in range(nb):
            newAlphabet.append(letter)
    return newAlphabet


if __name__ == "__main__":
    print(duplicateur(3))
    # ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'd', 'd', 'd', 'e', 'e', 'e',
    # 'f', 'f', 'f', 'g', 'g', 'g', 'h', 'h', 'h', 'i', 'i', 'i', 'j', 'j', 'j',
    # 'k', 'k', 'k', 'l', 'l', 'l', 'm', 'm', 'm', 'n', 'n', 'n', 'o', 'o', 'o',
    # 'p', 'p', 'p', 'q', 'q', 'q', 'r', 'r', 'r', 's', 's', 's', 't', 't', 't',
    # 'u', 'u', 'u', 'v', 'v', 'v', 'w', 'w', 'w', 'x', 'x', 'x', 'y', 'y', 'y', 'z', 'z', 'z']
