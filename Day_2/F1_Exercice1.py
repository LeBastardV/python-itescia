def chiffrePorteBonheur(nb):
    # Determiner si un chiffre est porte bonheur ou non.
    while True:
        resultat = 0
        for chiffre in str(nb):
            resultat += int(chiffre)*int(chiffre)
            print(chiffre, "^2=", int(chiffre)*int(chiffre), sep="")
        print("Nouveau nombre =", resultat)
        nb = resultat

        if resultat < 10:
            if resultat == 1:
                return True
            return False


if __name__ == "__main__":
    if chiffrePorteBonheur(913):
        print("Le nombre est porte bonheur")
    else:
        print("Le nombre n'est pas porte bonheur")
