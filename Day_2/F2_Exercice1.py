# Écrivez une classe Domino pour représenter une pièce de domino.
# Les objet sont initialisés avec les valeurs des deux faces, A et B.
# Ajoutez une méthode .affiche_points() qui affiche les valeurs des deux faces,
# et une méthode .totale() qui retourne la somme de deux valeurs.
# >>> d = Domino(4, 6)
# >>> d.affiche_points()
# face A: 4, face B: 6
# >>> x = d.totale()
# >>> print(x)
# 10

class Domino:

    def __init__(self, A, B):
        self.faceA = A
        self.faceB = B

    def affiche_points(self):
        print("Face A:", self.faceA, ",Face B:", self.faceB)

    def totale(self):
        return self.faceA + self.faceB

    def __repr__(self):
        return "Face A: {self.faceA}, Face B: {self.faceB}".format(self=self)


if __name__ == "__main__":
    dominoTest = Domino(4, 6)
    dominoTest.affiche_points()
    print(dominoTest.totale())
    print(dominoTest)
